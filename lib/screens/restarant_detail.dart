import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:testapp1/widgets/custom_app_bar.dart';
import 'package:getflutter/getflutter.dart';

class RestaurantDetail extends StatelessWidget {
  const RestaurantDetail({Key key, @required this.restaurant})
      : super(key: key);

  final Map<String, dynamic> restaurant;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar.defaultAppBar(restaurant['resTitle']),
      body: Container(
        child: ClipRRect(
          child: CachedNetworkImage(
            imageUrl: restaurant['resThumbnail'],
            imageBuilder: (context, imageProvider) => Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                image:
                    DecorationImage(image: imageProvider, fit: BoxFit.fitWidth),
              ),
            ),
            placeholder: (context, url) => Container(
              height: 60.0,
              child: Center(child: CircularProgressIndicator()),
            ),
            errorWidget: (context, url, error) => Icon(Icons.error),
          ),
        ),
      ),
    );
  }
}
