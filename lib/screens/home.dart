import 'package:flappy_search_bar/flappy_search_bar.dart';
import 'package:flappy_search_bar/scaled_tile.dart';
import 'package:flutter/material.dart';
import 'package:testapp1/screens/restaurant_map.dart';
import 'package:testapp1/widgets/restaurant_item.dart';
import 'package:testapp1/widgets/restaurant_list.dart';
import 'package:firebase_database/firebase_database.dart';

class RestaurantItemWidget {
  final RestaurantItem restaurant;

  RestaurantItemWidget(this.restaurant);
}

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final SearchBarController<RestaurantItemWidget> _searchBarController =
      SearchBarController();
  List<RestaurantItemWidget> searchResult = [];
  List<Map<String, dynamic>> restaurants = [];

  @override
  void initState() {
    super.initState();

    // Get restaurant list from firebase when initial state
    getRestaurants();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: SafeArea(
          child: SearchBar<RestaurantItemWidget>(
            hintText: 'Search for Restaurant',
            searchBarPadding: EdgeInsets.symmetric(horizontal: 10),
            headerPadding: EdgeInsets.symmetric(horizontal: 10),
            listPadding: EdgeInsets.symmetric(horizontal: 10),
            onSearch: _searchRestaurant,
            searchBarController: _searchBarController,
            placeHolder: restaurants.length > 0
                ? RestaurantList(
                    restaurants: restaurants,
                  )
                : null,
            cancellationWidget: Text("Cancel"),
            emptyWidget: Center(
              child: Text(
                "Not found restaurant",
                style: TextStyle(color: Colors.grey),
              ),
            ),
            indexedScaledTileBuilder: (int index) =>
                ScaledTile.count(1, index.isEven ? 2 : 1),
            header: Row(
              children: <Widget>[
                FlatButton.icon(
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => RestaurantMap(
                                restaurants: restaurants,
                              )));
                    },
                    icon: Icon(Icons.map, color: Colors.green),
                    label: Text('Restaurant nearby you'))
              ],
            ),
            onCancelled: () {},
            crossAxisCount: 1,
            onItemFound: (RestaurantItemWidget res, int index) {
              return ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight: 500.0,
                ),
                child: ListView.separated(
                    padding: EdgeInsets.only(top: 8.0),
                    shrinkWrap: true,
                    itemCount: searchResult.length,
                    itemBuilder: (context, index) {
                      return searchResult[index].restaurant;
                    },
                    separatorBuilder: (context, index) {
                      return Divider();
                    }),
              );
            },
          ),
        ),
      ),
    );
  }

  // ignore: todo
  // TODO: Get restaurant from Firebase
  getRestaurants() async {
    // ignore: await_only_futures
    DatabaseReference dbref =
        FirebaseDatabase.instance.reference().child("restaurants");

    List<Map<String, dynamic>> snapshotData = [];

    await dbref.once().then((DataSnapshot snapshot) {
      restaurants = [];

      for (var item in snapshot.value) {
        Map<String, dynamic> res = {
          "resId": item['resId'],
          "resTitle": item['resTitle'],
          "resSubTitle": item['resSubTitle'],
          "resLat": item['resLat'],
          "resLon": item['resLon'],
          "resThumbnail": item['resThumbnail'],
          "resWebsite": item['resWebsite'],
          "resTel": item['resTel'],
          "resRating": item['resRating']
        };
        snapshotData.add(res);
      }

      setState(() {
        restaurants = snapshotData;
      });
    });
  }

  // ignore: todo
  // TODO: Search for restaurant in array object
  Future<List<RestaurantItemWidget>> _searchRestaurant(String text) async {
    await Future.delayed(Duration(seconds: text.length < 3 ? 3 : 1));
    // Clear search result param
    searchResult = [];

    // Very simple search text in array
    restaurants.asMap().forEach((index, item) {
      if (item['resTitle']
          .toString()
          .toLowerCase()
          .contains(text.toLowerCase())) {
        searchResult.add(RestaurantItemWidget(RestaurantItem(
          restaurant: item,
        )));
      }
    });

    return searchResult;
  }
}
