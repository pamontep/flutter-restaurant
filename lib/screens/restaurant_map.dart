import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:testapp1/widgets/custom_app_bar.dart';
import 'package:url_launcher/url_launcher.dart';

class RestaurantMap extends StatefulWidget {
  const RestaurantMap({Key key, @required this.restaurants}) : super(key: key);
  final List<Map<String, dynamic>> restaurants;

  @override
  State<RestaurantMap> createState() => RestaurantMapState();
}

class RestaurantMapState extends State<RestaurantMap> {
  LocationData _currentLocation;
  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> _markers = Set();

  static final CameraPosition _initCamPos = CameraPosition(
    target: LatLng(13.866001, 100.496475), // Default value
    zoom: 14,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar.defaultAppBar('Find restaurant nearby you'),
      body: GoogleMap(
        mapType: MapType.normal,
        myLocationEnabled: true,
        initialCameraPosition: _initCamPos,
        onMapCreated: (GoogleMapController controller) {
          _myLocation();
          _initMarkers();
          _controller.complete(controller);
        },
        markers: _markers,
      ),
    );
  }

  // ignore: todo
  // TODO: Update latitude, longitude to current location
  Future<void> _myLocation() async {
    final GoogleMapController controller = await _controller.future;
    _currentLocation = await _getCurrentLocation();
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: LatLng(_currentLocation.latitude, _currentLocation.longitude),
      zoom: 14,
    )));
  }

  // ignore: todo
  // TODO: Get current location and prevent exception if User does not allow location
  Future<LocationData> _getCurrentLocation() async {
    Location location = Location();
    try {
      return await location.getLocation();
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        print('PERMISSION_DENIED');
      }
      return null;
    }
  }

  // ignore: todo
  // TODO: Generate markers on map
  Future<void> _initMarkers() async {
    setState(() {
      widget.restaurants.forEach((item) {
        _markers.add(Marker(
          markerId: MarkerId("$item['resId']"),
          position: LatLng(double.parse(item['resLat'].toString()),
              double.parse(item['resLon'].toString())),
          infoWindow: InfoWindow(
              title: item['resTitle'],
              snippet: item['resSubTitle'],
              onTap: () => _checkBallonLaunchInfo(item)),
        ));
      });
    });
  }

  // ignore: todo
  // TODO: Check to make a decision to launch in-app browser (official restaurant website) or if the link is not work, make a call
  _checkBallonLaunchInfo(Map<String, dynamic> restaurant) async {
    if (await canLaunch(restaurant['resWebsite'])) {
      await launch(restaurant['resWebsite']);
    } else {
      if (await canLaunch("tel://" + restaurant['resTel'])) {
        await launch("tel://" + restaurant['resTel']);
      } else {
        throw 'Could not launch $restaurant["resTel"]';
      }
      throw 'Could not launch $restaurant["resWebsite"]';
    }
  }
}
