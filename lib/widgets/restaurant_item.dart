import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:testapp1/screens/restarant_detail.dart';

class RestaurantItem extends StatelessWidget {
  const RestaurantItem({Key key, this.restaurant}) : super(key: key);
  final Map<String, dynamic> restaurant;

  @override
  Widget build(BuildContext context) {
    double _imageWidthPercentage = 0.35;

    return ListTile(
      leading: Container(
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          child: CachedNetworkImage(
            imageUrl: restaurant['resThumbnail'],
            imageBuilder: (context, imageProvider) => Container(
              width: MediaQuery.of(context).size.width * _imageWidthPercentage,
              decoration: BoxDecoration(
                image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
              ),
            ),
            placeholder: (context, url) => Container(
              height: 60.0,
              child: Center(child: CircularProgressIndicator()),
            ),
            errorWidget: (context, url, error) => Icon(Icons.error),
          ),
        ),
      ),
      title: Text(restaurant['resTitle']),
      subtitle: Text(restaurant['resSubTitle']),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            restaurant['resRating'].toString() + "/5.0",
            style: TextStyle(color: Colors.grey),
          ),
          Icon(
            (restaurant['resRating'] as double) > 4
                ? Icons.star
                : Icons.star_half,
            color: Colors.yellow,
          ),
        ],
      ),
      dense: false,
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => RestaurantDetail(
                  restaurant: restaurant,
                )));
      },
    );
  }
}
