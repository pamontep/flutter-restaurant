import 'package:flutter/material.dart';
import 'package:testapp1/widgets/restaurant_item.dart';

class RestaurantList extends StatelessWidget {
  const RestaurantList({Key key, @required this.restaurants}) : super(key: key);
  final List<Map<String, dynamic>> restaurants;

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(
        minHeight: 500.0,
      ),
      child: ListView.separated(
        padding: EdgeInsets.only(top: 8.0),
        shrinkWrap: true,
        itemCount: restaurants.length,
        itemBuilder: (context, index) {
          return RestaurantItem(restaurant: restaurants[index]);
        },
        separatorBuilder: (context, index) {
          return Divider();
        },
      ),
    );
  }
}
