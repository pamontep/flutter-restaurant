import 'package:flutter/material.dart';

class CustomAppBar {
  static defaultAppBar(String text) {
    return AppBar(
      title: Text(
        text,
        style: TextStyle(
            color: Colors.black, fontWeight: FontWeight.w500, fontSize: 16.0),
      ),
      centerTitle: true,
      backgroundColor: Colors.white,
      iconTheme: IconThemeData(color: Colors.grey),
      brightness: Brightness.light,
      elevation: 0.5,
    );
  }
}
