import 'package:flutter/material.dart';
import 'package:testapp1/screens/home.dart';

void main() => runApp(MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        fontFamily: 'Sarabun',
        hintColor: Colors.white,
      ),
      home: HomePage(),
    ));
