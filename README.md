# Flutter Restaurant Exam Application
This application developed by Flutter.

# Screenshot
<img src="/assets/screenshots/restaurant-listing.png" width="250">
<img src="/assets/screenshots/google-map.png" width="250">

# Demo video

Press for watch video

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/0o3XxH2Fg00/0.jpg)](https://www.youtube.com/watch?v=0o3XxH2Fg00)


# Installation
Checkout and enjoy

# Change Logs
### v0.1 (Sep 11th 2020)
* Restaurant listing (Pull the list from Firebase)
* Search for restaurant
* Google map with restaurant marker and press on the marker for open restaurant's website or make a call

# License

Copyright 2020 Pamontep Panya

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
